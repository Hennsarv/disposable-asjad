﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Destruktoriasjad
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Suur s = new Suur())
            { 

            }
        }
    }

    // ma käin PÕGUSALT üle, mis on
    // konstruktor (appi me ju teame)
    // destruktor (oot..oot)
    // Disposable interface
    // using blokk

    class Suur : IDisposable    
    {


        static int algus = 0;
        int asi = 0;
        public Suur()
        {
            asi = ++algus;
            Console.WriteLine("Tehti suur asi number {0}", asi);
        }

        ~Suur()
        {
//            Console.WriteLine("suur asi number {0} visati minema", asi);
            Dispose(false);
        }

        
        private int[] sisu = new int[1_000_000];

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            Console.WriteLine("Keegi tegi dispose {0}", disposing);

            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    GC.SuppressFinalize(this);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Suur() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

}
